<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;
use App\Phone;

class SavePhone extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		if(Request::segment(2) && !Phone::where('user_id', Auth::id())->find(Request::segment(2))) {
			return false;
		}

		return !Auth::guest();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required|min:2|max:255',
			'phone_number' => 'required|numeric'
		];
	}

}
