<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class Register extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::guest();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'username' => 'required|min:4|max:50|alpha_dash|unique:users,username',
			'password' => 'required|min:6',
			'repeat_password' => 'required|same:password'
		];
	}

}
