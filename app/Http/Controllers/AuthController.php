<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\Register;

use App\User;
use Auth;

class AuthController extends Controller {

	public function getLogin() {
		return view('auth.login', [
			'title' => 'Вход'
		]);
	}

	public function postLogin(Request $request) {
		if(Auth::attempt(['username' => $request->input('username'), 'password' => $request->input('password')])) {
			return redirect('/');
		}

		return redirect('auth/login')->with('message', [
			'type' => 'danger',
			'heading' => 'Невалидни данни за вход',
			'body' => 'Уверете се дали сте въвели правилни данни за вход!'
		]);
	}

	public function getRegister() {
		return view('auth.register', [
			'title' => 'Регистрация'
		]);
	}

	public function postRegister(Register $request) {
		$user = User::create([
			'username' => $request->input('username'),
			'password' => bcrypt($request->input('password'))
		]);

		if($user->id) {
			return redirect('auth/login')->with('message', [
				'type' => 'success',
				'heading' => 'Регистрацията е създадена успешно!',
				'body' => 'Сега можете да влезете в профила си!'
			]);
		}
	}

	public function getLogout() {
		Auth::logout();

		return redirect('auth/login')->with('message', [
			'type' => 'success',
			'body' => 'Излязохте успешно от профила си!'
		]);
	}

}
