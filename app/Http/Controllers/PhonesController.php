<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\SavePhone;
use App\Phone;

use Auth;

class PhonesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('phones.index', [
			'title' => 'Телефони',
			'phones' => Phone::where('user_id', Auth::id())->get(['id', 'name', 'phone_number'])
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('phones.create', [
			'title' => 'Добави телефонен номер'
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(SavePhone $request)
	{
		$phone = Phone::create([
			'user_id' => Auth::id(),
			'name' => $request->input('name'),
			'phone_number' => $request->input('phone_number')
		]);

		if(!$phone) {
			return redirect()->back()->withInput();
		}

		return redirect('phones')->with('message', [
			'type' => 'success',
			'body' => 'Телефонният номер е създаден успешно!'
		]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$phone = Phone::where('user_id', Auth::id())->find($id);

		if(!$phone) {
			return redirect('phones')->with('message', [
				'type' => 'danger',
				'body' => 'Телефонният номер не съществува.'
			]);
		}

		return view('phones.create', [
			'title' => $phone->name .' - Редактиране',
			'item' => $phone
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(SavePhone $request, $id)
	{
		$isUpdated = Phone::where('id', $id)->update([
			'name' => $request->input('name'),
			'phone_number' => $request->input('phone_number')
		]);

		return redirect()->back()->with('message', $isUpdated ? [
			'type' => 'success',
			'body' => 'Телефонният номер е обновен успешно!'
		] : [
			'type' => 'danger',
			'body' => 'Телефонният номер не е обновен. Моля, опитайте по-късно!'
		]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$phone = Phone::where('user_id', Auth::id())->find($id);
		return redirect()->back()->with('message', $phone && $phone->delete() ? [
			'type' => 'success',
			'body' => 'Телефонният номер е изтрит успешно!'
		] : [
			'type' => 'danger',
			'body' => 'Телефонният номер не е изтрит. Моля, опитайте по-късно!'
		]);
	}

}
