<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::controller('auth', 'AuthController');

Route::group([
    'middleware' => 'auth'
], function() {
    Route::get('/', function() {
        return redirect('phones');
    });
    Route::resource('phones', 'PhonesController');
    Route::get('phones/{id}/delete', 'PhonesController@destroy');
});
