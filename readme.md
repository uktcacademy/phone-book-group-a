# PhoneBook Group A
This application is a MVC practice in the [PHP Web Development Course](http://uktcacademy.com/courses/php-web-development). We built it with Group A.

## License
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/)

## What do you need?
* PHP >= 5.5.9
* [Composer](https://getcomposer.org)

## How to install?
1. Clone this repository to your computer.
2. Open terminal (or CMD if you are on Windows) in the same folder.
3. Execute "composer update" in the termnial.
4. Create database (the charset must be utf8_general_ci!) via phpMyAdmin or other tool.
5. Configure your database settings in your .env file.
6. Execute "php artisan migrate" in the terminal.
7. Execute "php artisan serve" to start a development web server.
8. Access your installation on http://localhost:8000
