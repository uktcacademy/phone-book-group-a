<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>{{ isset($title) ? $title.' - ' : ''}}Телефонен указател</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    </head>
    <body>
        <nav class="navbar navbar-default">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="{{ url() }}">Телефонен указател</a>
            </div>

            <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url() }}">Начало</a>
                </li>
                @if(Auth::guest())
                    <li>
                        <a href="{{ url('auth/login') }}">Вход</a>
                    </li>
                    <li>
                        <a href="{{ url('auth/register') }}">Регистрация</a>
                    </li>
                @else
                    <li>
                        <a href="{{ url('auth/logout') }}">Изход</a>
                    </li>
                @endif
              </ul>
            </div>
          </div>
        </nav>
        @section('header')
            @if(isset($title))
                <div class="container">
                    <div class="page-header">
                        <h1>{{ $title }}</h1>
                    </div>
                </div>
            @endif
        @show
        @if(Session::has('message'))
            <div class="container">
                <div class="alert alert-{{ Session::get('message.type', 'info') }}">
                	@if(Session::has('message.heading'))
                    	<h4>{{ Session::get('message.heading') }}</h4>
                    @endif
                    <p>{!! Session::get('message.body') !!}</p>
                </div>
            </div>
        @endif
        @yield('content')
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    </body>
</html>
