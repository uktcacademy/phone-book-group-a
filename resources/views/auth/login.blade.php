@extends('layouts.default')
@section('content')
    <div class="container">
        {!! Form::open([
            'method' => 'POST'
        ]) !!}
        <div class="form-group">
            <label for="username">Потребителско име:</label>
            {!!
                Form::text('username', null, [
                    'id' => 'username',
                    'class' => 'form-control'
                ])
            !!}
        </div>
        <div class="form-group">
            <label for="password">Парола:</label>
            {!!
                Form::password('password', [
                    'id' => 'password',
                    'class' => 'form-control'
                ])
            !!}
        </div>
        <div class="form-group">
            {!!
                Form::submit('Вход', [
                    'class' => 'btn btn-primary'
                ])
            !!}
            <a href="{{ url('auth/register') }}" class="btn btn-default">Регистрация</a>
        </div>

        {!! Form::close() !!}
    </div>
@stop
