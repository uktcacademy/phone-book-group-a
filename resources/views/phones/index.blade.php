@extends('layouts.default')
@section('header')
    <div class="container">
        <div class="page-header">
            <h1>
                {{ $title }}
                <a href="{{ url('phones/create') }}" class="btn btn-primary" style="margin-left: 10px;">Добави нов</a>
            </h1>
        </div>
    </div>
@stop
@section('content')
    <div class="container">
        @if(!$phones->count())
            <p class="alert alert-info">
                Не сте добавили никакви телефонни номера.
            </p>
        @else
            <div class="panel panel-default">
                <div class="panel-heading">
                    Списък
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>
                                    Име:
                                </th>
                                <th>
                                    Телефонен номер:
                                </th>
                                <th colspan="2" class="text-center">
                                    Опции:
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($phones as $phone)
                                <tr>
                                    <td>
                                        {{ $phone->name }}
                                    </td>
                                    <td>
                                        {{ $phone->phone_number }}
                                    </td>
                                    <td width="1">
                                        <a href="{{ url('phones/'.$phone->id.'/edit') }}" title="Редактиране" class="btn btn-warning btn-xs">
                                            <span class="glyphicon glyphicon-pencil"></span> Редактиране
                                        </a>
                                    </td>
                                    <td width="1">
                                        <a href="{{ url('phones/'.$phone->id.'/delete') }}" class="btn btn-danger btn-xs">
                                            <span class="glyphicon glyphicon-remove"></span> Изтриване
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    </div>
@stop
