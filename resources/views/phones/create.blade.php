@extends('layouts.default')
@section('content')
    <div class="container">
        {!! Form::open(['class' => 'form-horizontal', 'url' => (isset($item) ? 'phones/'.$item->id : 'phones'), 'method' => (isset($item) ? 'PUT' : 'POST')]) !!}
            <div class="form-group">
                {!! Form::label('name', 'Име', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    {!! Form::text('name', (isset($item) ? $item->name : null), ['class' => 'form-control']) !!}
                    {!! $errors->first('name') !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('phone_number', 'Телефонен номер', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    {!! Form::text('phone_number', (isset($item) ? $item->phone_number : null), ['class' => 'form-control']) !!}
                    {!! $errors->first('phone_number') !!}
                </div>
            </div>
            <div class="row">
                <div class="col col-sm-10 col-sm-offset-2">
                    {!! Form::submit('Запиши', ['class' => 'btn btn-primary']) !!}
                    <a href="{{ url('phones') }}" class="btn btn-default">Отказ</a>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@stop
