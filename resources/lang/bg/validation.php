<?php

return [
    'required' => 'Полето е задължително.',
    'required_with' => 'Полето е задължително.',
    'email' => 'Полето трябва да бъде валиден имейл адрес.',
    'min' => [
        "numeric" => 'Полето трябва да бъде поне :min.',
        "file"    => 'Полето трябва да бъде поне :min килобайта.',
        "string"  => 'Полето трябва да бъде поне :min символа.',
        "array"   => 'Полето трябва да съдържа поне :min елемента.',
    ],
    'max' => [
        "numeric" => 'Полето трябва да бъде най-много :max.',
        "file"    => 'Полето трябва да бъде най-много :max килобайта.',
        "string"  => 'Полето трябва да бъде най-много :max символа.',
        "array"   => 'Полето трябва да съдържа най-много :max елемента.',
    ],
    //The slug may only contain letters, numbers, and dashes.
    'alpha_dash' => 'Полето може да съдържа единствено букви, цифри и долна черта.',
    'date' => 'Невалидна дата.',
    'exists' => 'Полето е с невалидна стойност.',
    'not_exists' => 'Полето е с невалидна стойност.',
    'in' => 'Полето е с невалидна стойност.',
    'not_in' => 'Полето е с невалидна стойност.',
    'name' => 'Полето трябва да съдържа име и фамилия.',
    'numeric' => 'Невалиден телефонен номер.',
    'unique' => 'Стойността на полето вече е заета.',
    'same' => 'Двете тойности не съвпадат.',
    'url' => 'Невалиден URL адрес.',

    'custom' => [
        'email' => [
            'unique' => 'Имейлът вече се използва от друг потребител.',
            'not_exists' => 'Този имейл адрес вече е регистриран в платформата.',
            'exists' => 'Не съществува потребител с този имейл.'
        ],
        'repeat_password' => [
            'same' => 'Паролите не съвпадат.'
        ]
    ]
];
